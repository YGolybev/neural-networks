package input

import afunctions.EqualsFunction
import base.connection.BaseConnectionFactory
import base.network.BaseNeuron
import base.network.Neuron

/**
 * Нейрон входного слоя
 */
class InputNeuron :
        BaseNeuron(0f, EqualsFunction()) {

    override fun connect(neuron: Neuron<Float>) {
        connections.add(BaseConnectionFactory.connect(neuron, 1f))
    }

}