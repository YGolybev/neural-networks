package input

import base.network.NeuronsLayer

/**
 * Входной слой нейронной сети
 *
 * @param size количество нейронов на слое (обычно равен высота_изображения*ширина_изображения)
 */
class InputLayer(size: Int = 0) : NeuronsLayer(::InputNeuron) {
    init {
        fillWith(size)
    }

    override fun connectToFullyConnected(layer: NeuronsLayer) {
        neurons.forEachIndexed { i, neuron ->
            neuron.connect(layer.neurons[i])
        }
    }
}