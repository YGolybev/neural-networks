package base.connection

import base.misc.ConnectionFactory
import base.network.Neuron


object BaseConnectionFactory : ConnectionFactory<Float> {

    override fun connect(n: Neuron<Float>, weight: Float): Connection {
        return Connection(n, weight)
    }

}