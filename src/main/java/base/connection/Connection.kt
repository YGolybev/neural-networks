package base.connection

import base.network.Neuron

/**
 * Соединение между нейронами
 *
 * @param neuron выходной нейрон соединения
 * @param weight вес соединения
 */
class Connection(val neuron: Neuron<Float>, var weight: Float = 0f)