package base

import base.network.Layer
import base.network.NeuralNetwork
import base.network.NeuronsLayer
import base.studying.StudyAlgorithm
import cnn.ConvolutionalNeuralNetwork
import input.InputLayer
import input.InputNeuron
import kohonen.KohonenNeuralNetwork
import kohonen.KohonenStudyAlgorithm
import model.Image
import perceptron.Perceptron
import perceptron.PerceptronLayer
import perceptron.PerceptronStudyAlgorithm
import preprocess.Deskew
import preprocess.ImageContent
import preprocess.Scaling
import preprocess.SplitImage
import study.BackpropagationStudyAlgorithm
import java.io.File
import javax.imageio.ImageIO

const val KOHONEN = "koh"
const val CONVOLUTION = "con"
const val PERCEPTRON = "per"

const val BACKPROPAGATION = "backprop"

class NeuralNetworkTesterBuilder(val neuralNetwork: NeuralNetwork?, val trainSet: File?,
                                 val testSet: File?, val studyAlgorithm: StudyAlgorithm?,
                                 val learningRate: Float, val layers: List<Layer>,
                                 val trainSetContent: File?, val testSetContent: File?,
                                 val imageSize: Int, val isDeskew: Boolean,
                                 val isMse: Boolean, val mse: Float, val epochs: Int) {

    constructor() : this(null, null, null, null, 0f, listOf(), null, null, 0, false, false, 0f, 1)

    fun neuralNetwork(type: String): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(when (type.trim().toLowerCase()) {
            KOHONEN -> KohonenNeuralNetwork()
            CONVOLUTION -> ConvolutionalNeuralNetwork()
            PERCEPTRON -> Perceptron()
            else -> throw UnsupportedOperationException("Неверный тип нейронной сети")
        }, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun trainSet(ts: File): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, ts, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun trainSetContent(ts: File): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, ts, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun testSet(ts: File): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, ts, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun testSetContent(ts: File): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, ts, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun studyAlgorithm(type: String): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, when (type) {
            BACKPROPAGATION -> BackpropagationStudyAlgorithm(learningRate)
            PERCEPTRON -> PerceptronStudyAlgorithm()
            KOHONEN -> KohonenStudyAlgorithm(learningRate)
            else -> throw  UnsupportedOperationException("Неверный метод обучения")
        }, learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun learningRate(learningRate: Float): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun layers(layers: List<Layer>): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun imageSize(size: Int): NeuralNetworkTesterBuilder {
        if (size <= 2) {
            throw Exception()
        }
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, size, isDeskew, isMse, mse, epochs)
    }

    fun deskew(isEnabled: Boolean): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isEnabled, isMse, mse, epochs)
    }

    fun mse(mse: Float): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, true, mse, epochs)
    }

    fun epochs(epochs: Int): NeuralNetworkTesterBuilder {
        return NeuralNetworkTesterBuilder(neuralNetwork, trainSet, testSet, studyAlgorithm,
                learningRate, layers, trainSetContent, testSetContent, imageSize, isDeskew, isMse, mse, epochs)
    }

    fun build(): NeuralNetworkTester {
        val scaling = Scaling(imageSize)
        val imageContent = ImageContent()
        val splitImage = SplitImage()
        val deskew = Deskew()

        println("Разделяем обучающую выборку...")
        val trsA = imageContent.preprocess(trainSetContent!!)
        val trs = splitImage.preprocess(Image.Factory.create(trainSet!!.absolutePath, 0)).mapIndexed { i, image ->
            scaling.preprocess(Image(image.data, trsA[i], image.width, image.height))
        }

        println("Разделяем тестовую выборку...")
        val tssA = imageContent.preprocess(testSetContent!!)
        val testBufferedImage = ImageIO.read(testSet)
        val testImage = if (isDeskew) deskew.preprocess(testBufferedImage) else Image.Factory.create(testBufferedImage, 0)
        val tss = splitImage.preprocess(testImage).mapIndexed { i, image ->
            scaling.preprocess(Image(image.data, tssA[i], image.width, image.height))
        }

        val inputSize = imageSize * imageSize

        val inputLayers = listOfNotNull(InputLayer(inputSize),
                if (this.layers.isEmpty() || this.layers.first() is NeuronsLayer)
                    NeuronsLayer(::InputNeuron).apply { fillWith(inputSize) }
                else null)


        val layers = inputLayers +
                this.layers + PerceptronLayer(11)

        return NeuralNetworkTester(when (neuralNetwork) {
            is ConvolutionalNeuralNetwork -> ConvolutionalNeuralNetwork(layers,
                    studyAlgorithm as BackpropagationStudyAlgorithm)
            is Perceptron -> Perceptron(layers, studyAlgorithm!!)
            else -> KohonenNeuralNetwork(layers, studyAlgorithm!!)
        }, trs, tss, isMse, mse, epochs)
    }

}