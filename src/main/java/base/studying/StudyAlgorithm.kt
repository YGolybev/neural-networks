package base.studying

import base.network.NeuralNetwork
import model.Image

/**
 * Алгоритм для обучения искусственной нейронной сети
 */
interface StudyAlgorithm {

    /**
     * Обучает искусственную нейронную сеть
     *
     * @param network нейронная сеть [NeuralNetwork] которую необходимо обучить
     * @param images список изображений на которых нейронная сеть обучается
     * @param initializeWeights инициализирвать ли веса нейронной сети (необходимо при первом этапе обучения)
     */
    fun study(network: NeuralNetwork, images: List<Image>, initializeWeights: Boolean = false)

}