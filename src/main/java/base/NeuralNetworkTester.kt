package base

import base.network.NeuralNetwork
import model.Image

/**
 * Тестирует нейронную сеть
 *
 * @property neuralNetwork нейронная сеть, которую необходимо протестировать
 * @property trainSet Обучающая выборка на которой [neuralNetwork] будет обучаться
 * @property testSet
 */
class NeuralNetworkTester(val neuralNetwork: NeuralNetwork,
                          val trainSet: List<Image>,
                          val testSet: List<Image>,
                          val stopByMse: Boolean,
                          val mse: Float,
                          val epochs: Int) {

    fun test(): String {
        var epochsPassed = 0

        println("Обучение начато...")
        do {
            neuralNetwork.study(trainSet, epochsPassed == 0)

            val currentMse = trainSet.fold(0f) { acc, img ->
                val e = (img.answer - neuralNetwork.handle(img).toInt())
                acc + e * e
            } / trainSet.size

            epochsPassed += 1
            println("Эпох обучения прошло $epochsPassed, среднеквадратическая ошибка $currentMse")
        } while ((!stopByMse && epochsPassed < epochs) || (stopByMse && currentMse > mse))

        println("Тестирование...")
        val errorRate = testSet.fold(0) { acc, img ->
            val ans = neuralNetwork.handle(img).toInt()
            print("$ans : ${img.answer}, ")
            acc + if (ans == img.answer) 1 else 0
        }
        println()

        val mse = testSet.fold(0f) { acc, img ->
            val e = img.answer - neuralNetwork.handle(img)
            acc + e * e
        } / testSet.size

        return "Среднеквадратическая ошибка тестового набора $mse;\n" +
                "Количество верных ответов $errorRate из ${testSet.size}"
    }

}