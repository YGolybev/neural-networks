package base.misc

import base.connection.Connection
import base.network.Neuron

/**
 * Описывает фабрику для создания соединений [Connection] между нейронами
 */
interface ConnectionFactory<T : Number> {

    /**
     * Создает соединение [Connection] с нейроном
     *
     * @param n нейрон с которым необходимо создать соединение
     * @param weight изначальный вес соединения
     *
     * @return соединение с нейроном n
     */
    fun connect(n: Neuron<T>, weight: T): Connection

}