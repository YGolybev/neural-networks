package base.misc

/**
 * Описывает фунцию, которую можно использовать как функциюю активации нейрона
 */
interface ActivationFunction {

    /**
     * Активирует функцию
     *
     * @param v значение для передачи в функцию
     * @return значение возвращаемое функцией
     */
    fun activate(v: Float): Float

    /**
     * Производная функции
     *
     * @param v значение на котором необходимо вычислить производную
     * @return значение производной данной функции в задданой точке
     */
    fun derivative(v: Float): Float

}