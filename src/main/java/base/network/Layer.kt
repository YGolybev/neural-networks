package base.network


/**
 * Описывает слой искусственной нейронной сети
 */
interface Layer {

    /**
     * Активирует слой
     */
    fun activate()

    /**
     * Соединяет текущий слой с заданным
     * @param layer слой с которым необходимо соединить текущий слой
     */
    fun connect(layer: Layer)

    /**
     * Заполняет слой нейронами
     * @param count количество нейронов, которыми надо заполнить слой
     */
    fun fillWith(count: Int)

    /**
     * Сбрасывает значения у нейронов слоя
     */
    fun clear()

}