package base.network

import model.Image
import java.util.*

/**
 * Базовый фукнционал искусственной нейронной сети
 */
abstract class BaseNeuralNetwork(layers: List<Layer>) : NeuralNetwork {

    override final val layers = ArrayList<Layer>()

    init {
        this.layers += layers
        layers.take(layers.size - 1).forEachIndexed { i, layer -> layer.connect(layers[i + 1]) }
    }

    override fun study(images: List<Image>, initializeWeights: Boolean) {
        studyAlgorithm.study(this, images, initializeWeights)
    }

    override fun handle(img: Image, toStudy: Boolean): Float {
        (layers.first() as NeuronsLayer).neurons.forEachIndexed { i, neuron ->
            neuron.signal(img.data.data[i].toFloat())
        }

        layers.take(layers.size - 1).forEach(Layer::activate)

        val r = (layers.last() as NeuronsLayer).let {
            it.neurons.indexOf(it.neurons.maxBy(Neuron<Float>::activation))
        }

        if (!toStudy) {
            clear()
        }

        return r.toFloat()
    }

    override fun clear() {
        layers.forEach(Layer::clear)
    }
}