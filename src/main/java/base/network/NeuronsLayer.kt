package base.network

import cnn.FeatureMap
import cnn.FeatureMapLayer
import java.util.*

/**
 * Полносвязный слой искуственной нейронной сети
 *
 * @property neuronFactory функция которая создает нейроны [Neuron] которые будут использованы на данном слое
 */
open class NeuronsLayer(val neuronFactory: () -> Neuron<Float>) : Layer {

    /**
     * Список нейронов на данном слое
     */
    val neurons = ArrayList<Neuron<Float>>()

    override fun activate() {
        neurons.forEach(Neuron<Float>::activate)
    }

    override fun connect(layer: Layer) {
        when (layer) {
            is NeuronsLayer -> connectToFullyConnected(layer)
            is FeatureMapLayer -> connectToFMLayer(layer)
        }
    }

    private fun connectToFMLayer(layer: FeatureMapLayer) {
        layer.ifm += FeatureMap(Math.sqrt(neurons.size.toDouble()).toInt())

        neurons.forEachIndexed { i, neuron ->
            neuron.connect(layer.ifm.first().data[i])
        }
    }

    protected open fun connectToFullyConnected(layer: NeuronsLayer) {
        neurons.forEach { n ->
            layer.neurons.forEach { n2: Neuron<Float> ->
                n.connect(n2)
            }
        }
    }

    override fun fillWith(count: Int) {
        for (i in 1..count) {
            neurons.add(neuronFactory())
        }
    }

    override fun clear() {
        neurons.forEach(Neuron<Float>::clear)
    }

}