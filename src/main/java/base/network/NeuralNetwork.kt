package base.network

import base.studying.StudyAlgorithm
import model.Image

/**
 * Описывает искусственную нейронную сеть.
 */
interface NeuralNetwork {

    /**
     * Слои нейронной сети
     */
    val layers: List<Layer>

    /**
     * Алгоритм для обучения нейронной сети
     */
    val studyAlgorithm: StudyAlgorithm

    /**
     * Метод для обучения нейронной сети
     *
     * @param images изображения на которых необходимо производить обучение
     * @param initializeWeights инициализировать ли веса нейронной сети. Необходимо производить при первом проходе обучения.
     */
    fun study(images: List<Image>, initializeWeights: Boolean = false)

    /**
     * Подает изображение нейронной сети для определения что на ней
     *
     * @param img изображение для которого необходимо определить содержимое
     * @param toStudy если true, то метод clear не будет вызван, по окончании определения
     * @return ответ нейронной сети
     */
    fun handle(img: Image, toStudy: Boolean = false): Float

    /**
     * Обнулить значение power на нейронах
     */
    fun clear()

}