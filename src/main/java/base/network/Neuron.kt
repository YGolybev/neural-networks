package base.network

import base.connection.Connection
import base.misc.ActivationFunction
import java.util.*

/**
 * Описывает искуственный нейрон
 * @param T какой тип хранится сумматорои нейрона
 */
interface Neuron<T : Number> {

    /**
     * Накопленное значение на сумматоре нейрона
     */
    var power: T

    /**
     * Список соединений [Connection] с другими нейронам
     */
    val connections: ArrayList<Connection>

    /**
     * Метод для обработки поданного на нейрон сигнала
     */
    fun signal(s: T)

    /**
     * Активация нейрона
     */
    fun activate()

    /**
     * Значение функции активации нейрона
     * @return значение функция активации нейрона
     */
    fun activation(): T

    /**
     * Соединяет нейрон с другим нейроном, такого же типа
     */
    fun connect(neuron: Neuron<T>)

    /**
     * Очищает значение на нейроне
     */
    fun clear()

    /**
     * Функция активации нейрона [ActivationFunction]
     * @return функция активации нейрона
     */
    fun activationFunction(): ActivationFunction

}