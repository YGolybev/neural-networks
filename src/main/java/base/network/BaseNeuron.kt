package base.network

import base.connection.BaseConnectionFactory
import base.connection.Connection
import base.misc.ActivationFunction
import java.util.*

/**
 * Базовый нейрон
 *
 * @param activationFunction функция активации данного нейрона
 */
open class BaseNeuron(override var power: Float = 0f, val activationFunction: ActivationFunction) : Neuron<Float> {

    override val connections = ArrayList<Connection>()

    override fun signal(s: Float) {
        power += s
    }

    override fun activate() {
        connections.forEach {
            it.neuron.signal(activation() * it.weight)
        }
    }

    override fun activation(): Float {
        return activationFunction.activate(power)
    }

    override fun connect(neuron: Neuron<Float>) {
        connections.add(BaseConnectionFactory.connect(neuron, 1f))
    }

    override fun clear() {
        power = 0f
    }

    override fun activationFunction() = activationFunction

    override fun toString(): String {
        return "P: $power : C: ${connections.size}"
    }

}