package cnn.convolution

import java.util.*


class ConvFilter(val size: Int) {

    val connections = ArrayList<ConvConnection>()

    init {
        connections += (1..size * size).asIterable().map { ConvConnection((Math.random() - 0.5).toFloat()) }
    }

    override fun toString(): String {
        return connections.foldIndexed("") { i, acc, c ->
            acc + (if (i.mod(size) == 0) "\n" else "") + String.format("%.2f", c.weight) + " : "
        }
    }

}