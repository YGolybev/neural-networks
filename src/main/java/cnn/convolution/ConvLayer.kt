package cnn.convolution

import afunctions.ReluFunction
import base.network.BaseNeuron
import base.network.Layer
import base.network.NeuronsLayer
import cnn.FeatureMap
import cnn.FeatureMapLayer
import cnn.relu.ReluLayer
import cnn.subsampling.SubsamplingLayer
import java.util.*

class ConvLayer(val filterSize: Int, val filterAmount: Int) : FeatureMapLayer {

    override val ifm = ArrayList<FeatureMap>()

    val fs = ArrayList<ConvFilter>()

    init {
        (1..filterAmount).mapTo(fs) { ConvFilter(filterSize) }
    }

    override fun activate() {
        fs.forEach { filter ->
            filter.connections.forEachIndexed { i, convConnection ->
                convConnection.neuronsOut.forEachIndexed { nIndex, neuron ->
                    neuron.power += convConnection.weight * convConnection.neuronsIn[nIndex].activation()
                }
            }
        }
    }

    override fun connect(layer: Layer) {
        when (layer) {
            is ConvLayer -> connectToConv(layer)
            is NeuronsLayer -> connectToFullyConnected(layer)
            is SubsamplingLayer -> connectToConv(layer)
            is ReluLayer -> connectToReluLayer(layer)
        }
    }

    fun connectToFullyConnected(layer: NeuronsLayer) {
        val connectionsPerFeatureMap = (layer.neurons.size / ifm.size).toInt()

        ifm.forEachIndexed { fmIndex, featureMap ->
            val l = ArrayList<FeatureMap>()
            (0..featureMap.size - filterSize).forEach { i ->
                (0..featureMap.size - filterSize)
                        .mapTo(l) { j -> featureMap.getPartialFeatureMap(j, i, filterSize) }
            }

            for (cpr in 0..connectionsPerFeatureMap - 1)
                l.forEachIndexed { lFmIndex, lFeatureMap ->
                    val filterIndex = fmIndex * connectionsPerFeatureMap + cpr
                    val filter = fs[filterIndex]

                    lFeatureMap.data.forEachIndexed { nIndex, neuron ->
                        val con = filter.connections[nIndex].neuronsIn
                        if (!con.contains(neuron)) {
                            con += neuron
                        }
                    }

                    filter.connections.forEachIndexed { i, convConnection ->
                        val con = convConnection.neuronsOut
                        if (!con.contains(layer.neurons[filterIndex])) {
                            con += layer.neurons[filterIndex]
                        }
                    }
                }

        }
    }

    fun connectToConv(layer: FeatureMapLayer) {
        (1..filterAmount).asIterable().mapTo(layer.ifm) { FeatureMap(ifm.first().size - filterSize + 1) }

        val filtersPerFeatureMap = (filterAmount / ifm.size).toInt()

        ifm.forEachIndexed { fmIndex, featureMap ->
            val l = ArrayList<FeatureMap>()
            (0..featureMap.size - filterSize).forEach { i ->
                (0..featureMap.size - filterSize)
                        .mapTo(l) { j -> featureMap.getPartialFeatureMap(j, i, filterSize) }
            }

            for (i in 0..filtersPerFeatureMap - 1) {
                l.forEachIndexed { lFmIndex, lFeatureMap ->
                    val filterIndex = fmIndex * filtersPerFeatureMap + i
                    val filter = fs[filterIndex]

                    lFeatureMap.data.forEachIndexed { nIndex, neuron ->
                        val con = filter.connections[nIndex].neuronsIn
                        if (!con.contains(neuron))
                            con += neuron
                    }

                    filter.connections.forEach {
                        val n = layer.ifm[filterIndex].data[lFmIndex]
                        if (!it.neuronsOut.contains(n))
                            it.neuronsOut += n
                    }
                }
            }
        }
    }

    fun connectToReluLayer(layer: ReluLayer) {
        (1..filterAmount).asIterable().mapTo(layer.ifm) {
            FeatureMap(ifm.first().size - filterSize + 1, { BaseNeuron(0f, ReluFunction()) })
        }

        val connectionsPerFilter = (filterAmount / ifm.size).toInt()

        ifm.forEachIndexed { fmIndex, featureMap ->
            val l = ArrayList<FeatureMap>()
            (0..featureMap.size - filterSize).forEach { i ->
                (0..featureMap.size - filterSize)
                        .mapTo(l) { j -> featureMap.getPartialFeatureMap(j, i, filterSize) }
            }

            for (i in 0..connectionsPerFilter - 1) {
                l.forEachIndexed { lFmIndex, lFeatureMap ->
                    val filterIndex = fmIndex * connectionsPerFilter + i
                    val filter = fs[filterIndex]

                    lFeatureMap.data.forEachIndexed { nIndex, neuron ->
                        val con = filter.connections[nIndex].neuronsIn
                        if (!con.contains(neuron))
                            con += neuron
                    }

                    filter.connections.forEach {
                        val con = it.neuronsOut
                        if (!con.contains(layer.ifm[filterIndex].data[lFmIndex]))
                            con += layer.ifm[filterIndex].data[lFmIndex]
                    }
                }
            }
        }

    }

    override fun fillWith(count: Int) {
    }

    override fun clear() {
        ifm.forEach { it.data.forEach { it.clear() } }
    }
}