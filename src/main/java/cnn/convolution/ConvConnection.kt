package cnn.convolution

import base.network.Neuron
import java.util.*


class ConvConnection(var weight: Float = 0f) {

    val neuronsOut = ArrayList<Neuron<Float>>()

    val neuronsIn = ArrayList<Neuron<Float>>()

}