package cnn

import base.network.Layer
import java.util.*


interface FeatureMapLayer : Layer {

    val ifm: ArrayList<FeatureMap>

}