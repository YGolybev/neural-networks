package cnn.subsampling

import base.network.Layer
import base.network.Neuron
import cnn.FeatureMap
import cnn.FeatureMapLayer
import java.util.*


class SubsamplingLayer(val poolSize: Int) : FeatureMapLayer {

    override val ifm = ArrayList<FeatureMap>()

    val poolingUnits = ArrayList<PoolingUnit>()

    override fun activate() {
        poolingUnits.forEach { pu ->
            pu.outNeuron.signal(pu.neuronsIn.maxBy { it.power }!!.power)
        }
    }

    override fun connect(layer: Layer) {
        when (layer) {
            is FeatureMapLayer -> connectToFMLayer(layer)
        }
    }

    fun connectToFMLayer(layer: FeatureMapLayer) {
        val poolsCount = ifm.first().size / poolSize

        (1..ifm.size).asIterable().mapTo(layer.ifm) { FeatureMap(poolsCount) }

        ifm.forEachIndexed { ifmIndex, featureMap ->
            val partialFeatureMaps = (0..poolsCount - 1).flatMap { i ->
                (0..poolsCount - 1).map { j ->
                    featureMap.getPartialFeatureMap(j * poolSize, i * poolSize, poolSize)
                }
            }

            partialFeatureMaps.mapIndexedTo(poolingUnits) { i, fm ->
                val poolingUnit = PoolingUnit(layer.ifm[ifmIndex].data[i])
                poolingUnit.neuronsIn += fm.data
                poolingUnit
            }
        }
    }

    override fun fillWith(count: Int) {
    }

    override fun clear() {
        ifm.forEach { it.data.forEach(Neuron<Float>::clear) }
    }

}