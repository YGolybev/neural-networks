package cnn.relu

import base.network.Layer
import base.network.Neuron
import cnn.FeatureMap
import cnn.FeatureMapLayer
import java.util.*


class ReluLayer : FeatureMapLayer {

    override val ifm = ArrayList<FeatureMap>()

    override fun activate() {
        ifm.forEach { it.data.forEach { it.activate() } }
    }

    override fun connect(layer: Layer) {
        when (layer) {
            is FeatureMapLayer -> connectToFMLayer(layer)
        }
    }

    fun connectToFMLayer(layer: FeatureMapLayer) {
        (1..ifm.size).asIterable().mapTo(layer.ifm) { FeatureMap(ifm.first().size) }

        ifm.forEachIndexed { fmIndex, featureMap ->
            featureMap.data.forEachIndexed { i, neuron -> neuron.connect(layer.ifm[fmIndex].data[i]) }
        }
    }

    override fun fillWith(count: Int) {
    }

    override fun clear() {
        ifm.forEach { it.data.forEach(Neuron<Float>::clear) }
    }
}