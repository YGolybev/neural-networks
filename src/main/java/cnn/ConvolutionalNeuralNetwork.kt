package cnn

import base.network.BaseNeuralNetwork
import base.network.Layer
import cnn.convolution.ConvLayer
import cnn.relu.ReluLayer
import cnn.subsampling.SubsamplingLayer
import input.InputLayer
import perceptron.PerceptronLayer
import study.BackpropagationStudyAlgorithm


class ConvolutionalNeuralNetwork(layers: List<Layer> = listOf(
        InputLayer(18 * 18),
        ConvLayer(3, 8),
        ReluLayer(),
        ConvLayer(3, 16),
        SubsamplingLayer(2),
        ConvLayer(5, 32),
        ConvLayer(3, 64),
        PerceptronLayer(64),
        PerceptronLayer(11)
), override val studyAlgorithm: BackpropagationStudyAlgorithm = BackpropagationStudyAlgorithm(0.5f))
    : BaseNeuralNetwork(layers)