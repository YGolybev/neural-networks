package cnn

import afunctions.EqualsFunction
import base.network.BaseNeuron
import base.network.Neuron
import java.util.*


class FeatureMap(val size: Int, neuronFactory: () -> Neuron<Float> = { BaseNeuron(0f, EqualsFunction()) }) {

    val data = ArrayList<Neuron<Float>>(size * size)

    init {
        for (i in 1..size * size) {
            data += neuronFactory()
        }
    }

    fun get(x: Int, y: Int): Neuron<Float> {
        return data[(y * size + x)]
    }

    fun set(x: Int, y: Int, value: Neuron<Float>) {
        data[(y * size + x)] = value
    }

    fun getPartialFeatureMap(x: Int, y: Int, size: Int): FeatureMap {
        val result = FeatureMap(size)
        for (i in x..x + size - 1) {
            for (j in y..y + size - 1) {
                result.set(i - x, j - y, this.get(i, j))
            }
        }
        return result
    }

    override fun toString(): String {
        val s = data.foldIndexed("", { i: Int, acc: String, t ->
            acc + if (i.mod(size) == 0) {
                "\n"
            } else {
                ""
            } + String.format("%.2f:", t.power)
        })
        return s
    }

}