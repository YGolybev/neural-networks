package study

import base.network.Layer
import base.network.NeuralNetwork
import base.network.Neuron
import base.network.NeuronsLayer
import base.studying.StudyAlgorithm
import cnn.convolution.ConvLayer
import cnn.relu.ReluLayer
import cnn.subsampling.SubsamplingLayer
import model.Image

/**
 * Метод обратного распространения ошибки
 *
 * @param learningRate скорость обучения (рекомендуется значение на интервале (0; 1)
 */
class BackpropagationStudyAlgorithm(val learningRate: Float = 1f) : StudyAlgorithm {

    override fun study(network: NeuralNetwork, images: List<Image>, initializeWeights: Boolean) {
        if (initializeWeights) {
            weightInit(network.layers.takeLast(network.layers.size - 1))
        }

        val reversedLayers = network.layers.reversed()

        images.forEachIndexed { i, image ->
            study(network, image, reversedLayers)
        }
    }

    private fun study(network: NeuralNetwork, image: Image, layers: List<Layer>) {
        network.handle(image, true)

        val takeLayers = network.layers.size - 1

        val errors = layers.take(takeLayers)
                .foldIndexed(mapOf<Neuron<Float>, Float>()) { layerIndex, acc, layer ->
                    acc + when (layer) {
                        is NeuronsLayer -> errorsForFullyConnectedLayer(image, layer, acc, layerIndex == 0)
                        is ConvLayer -> errorsForConvolutionLayer(layer, acc)
                        is SubsamplingLayer -> errorsInSubsamplingLayer(layer, acc)
                        is ReluLayer -> errorForReluLayer(layer, acc)
                        else -> mapOf()
                    }
                }

        network.layers.takeLast(takeLayers).forEach {
            when (it) {
                is NeuronsLayer -> studyFullyConnectedLayer(it, errors)
                is ConvLayer -> studyConvolutionLayer(it, errors)
            }
        }

        network.clear()
    }

    private fun errorsForFullyConnectedLayer(image: Image, layer: NeuronsLayer,
                                             errors: Map<Neuron<Float>, Float>,
                                             isFirst: Boolean): Map<Neuron<Float>, Float> {

        return layer.neurons.associateBy({ it }) { n ->
            val error = if (isFirst) {
                val answer = if (layer.neurons.indexOf(n) == image.answer) 1f else 0f
                (answer - n.activation())
            } else {
                n.connections.fold(0f) { acc, c -> acc + errors.getOrElse(c.neuron, { 0f }) * c.weight }
            }

            error * n.activationFunction().derivative(n.power)
        }
    }

    private fun errorForReluLayer(layer: ReluLayer, errors: Map<Neuron<Float>, Float>)
            : Map<Neuron<Float>, Float> {
        return layer.ifm.fold(mapOf<Neuron<Float>, Float>()) { acc, fm ->
            acc + fm.data.associateBy({ it }) { n ->
                val errorSum = errors.getOrElse(n.connections[0].neuron, { 0f })

                errorSum * n.activationFunction().derivative(n.power)
            }
        }
    }

    private fun errorsForConvolutionLayer(layer: ConvLayer, errors: Map<Neuron<Float>, Float>): Map<Neuron<Float>, Float> {
        return layer.ifm.fold(mapOf<Neuron<Float>, Float>()) { acc, fm ->
            acc + fm.data.associateBy({ it }) { n ->
                val errorSum =
                        layer.fs.filter { it.connections.filter { it.neuronsIn.contains(n) }.isNotEmpty() }
                                .fold(0f) { acc, filter ->
                                    acc + filter.connections.fold(0f) { acc, c ->
                                        val indexInFilter = c.neuronsIn.indexOf(n)
                                        acc + (c.neuronsOut
                                                .filterIndexed { i, neuron -> i == indexInFilter }
                                                .fold(0f) { acc, ne ->
                                                    acc + (errors.getOrElse(ne, { 0f })) //* c.weight
                                                } * c.weight)
                                    }
                                }

                errorSum * n.activationFunction().derivative(n.power)
            }
        }
    }

    private fun errorsInSubsamplingLayer(layer: SubsamplingLayer, errors: Map<Neuron<Float>, Float>)
            : Map<Neuron<Float>, Float> {
        return layer.ifm.fold(mapOf<Neuron<Float>, Float>()) { acc, fm ->
            acc + fm.data.associateBy({ it }) { n ->
                val neuronPoolingUnit = layer.poolingUnits.find { it.neuronsIn.contains(n) }!!

                errors.getOrElse(neuronPoolingUnit.outNeuron, { 0f }) * (if (n.power == neuronPoolingUnit.outNeuron.power) 1f else 0f)
            }
        }
    }

    private fun studyFullyConnectedLayer(layer: NeuronsLayer, errors: Map<Neuron<Float>, Float>) {
        layer.neurons.forEach { n ->
            val e = n.activation() * learningRate
            n.connections.forEach {
                it.weight += errors.getOrElse(it.neuron, { 0f }) * e
            }
        }
    }

    private fun studyConvolutionLayer(layer: ConvLayer, errors: Map<Neuron<Float>, Float>) {
        layer.fs.forEach {
            it.connections.forEach { c ->
                c.neuronsOut.forEachIndexed { i, n ->
                    val err = errors.getOrElse(n, { 0f }) * learningRate
                    c.weight += c.neuronsIn.fold(0f) { acc, ne ->
                        acc + ne.activation()
                    } * err
                }
            }
        }
    }

    private fun weightInit(layers: List<Layer>) {
        layers.forEach {
            if (it is NeuronsLayer) {
                it.neurons.forEach {
                    it.connections.forEach {
                        it.weight = Math.random().toFloat() - 0.5f
                    }
                }
            }
        }
    }

}