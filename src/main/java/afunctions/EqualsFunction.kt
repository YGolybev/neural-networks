package afunctions

import base.misc.ActivationFunction


/**
 * Тождественная функция активации. f(x) = x
 */
class EqualsFunction : ActivationFunction {

    override fun activate(v: Float): Float {
        return v
    }

    override fun derivative(v: Float): Float {
        return 1f
    }

}

