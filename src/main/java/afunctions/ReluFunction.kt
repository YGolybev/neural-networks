package afunctions

import base.misc.ActivationFunction

/**
 * Функция активации для ReLU слоя. f(x) = max(0, x)
 */
class ReluFunction : ActivationFunction {

    override fun activate(v: Float) = Math.max(0f, v)

    override fun derivative(v: Float) = if (v >= 0f) 1f else 0f

}