package afunctions

import base.misc.ActivationFunction

/**
 * Ступенчатая функция.
 * @param t означает порог при котором f(x) = 1
 */
class StepFunction(val t: Float = 0f) : ActivationFunction {

    override fun activate(v: Float): Float {
        return if (v >= t) 1f else -1f
    }

    override fun derivative(v: Float): Float {
        return if (v == t) 1f else 0f
    }

}