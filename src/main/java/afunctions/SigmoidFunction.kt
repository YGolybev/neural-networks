package afunctions

import base.misc.ActivationFunction

/**
 * Функция активации сигмоид. f(x) = 1/(1 + e^(-x))
 */
class SigmoidFunction : ActivationFunction {

    override fun activate(v: Float): Float {
        return (1.0 / (1.0 + Math.exp(-v.toDouble()))).toFloat()
    }

    override fun derivative(v: Float): Float {
        return activate(v).let { it * (1f - it) }
    }


}