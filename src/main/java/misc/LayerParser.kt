package misc

import base.network.Layer
import cnn.convolution.ConvLayer
import cnn.relu.ReluLayer
import cnn.subsampling.SubsamplingLayer
import perceptron.PerceptronLayer

/**
 * Объект для распознавания введенной топологии слоя
 */
object LayerParser {

    /**
     * Преобразовывает строку в список слоев для искусственной нейронной сети
     *
     * @param s строка, которую необходимо разобрать
     * @return список слоев заданных в строке [s]
     */
    fun parser(s: String): List<Layer> {
        return s.toLowerCase().split(",").map {
            val tkns = it.trim().split(":")
            when (tkns[0]) {
                "c" -> ConvLayer(tkns[1].toInt(), tkns[2].toInt())
                "r" -> ReluLayer()
                "s" -> SubsamplingLayer(tkns[1].toInt())
                "f" -> PerceptronLayer(tkns[1].toInt())
                else -> throw UnsupportedOperationException("Неверный тип слоя ${tkns[0]}")
            }
        }
    }

}