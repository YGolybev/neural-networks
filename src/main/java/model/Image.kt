package model

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
 * Черно-белое изображение для подачи нейронной сети
 *
 * @param data матрица [Matrix] для хранения значений пикселей изображения
 * @param answer цифра, которая содержится на изображении или 10, если на изображении не цифра
 * @param width ширина изображения
 * @param height высота изображения
 */
class Image(val data: Matrix<Int>,
            val answer: Int = 0,
            val width: Int = 50,
            val height: Int = width) {

    /**
     * Получает цвет пикселя изображения
     *
     * @param x значение X-координаты изображения, на которой находится пиксель
     * @param y значение Y-координаты изображения, на которой находится пиксель
     * @return количество черного на изображении на интервале [0;255], где 0 - белый, а 255 - черный
     */
    fun getPixel(x: Int, y: Int): Int {
        return data.get(x, y)
    }

    fun getPixelForPrint(x: Int, y: Int): String {
        return if (getPixel(x, y) == 0) " " else "1"
    }

    fun print() {
        for (i in 0..width - 1) {
            for (j in 0..width - 1) {
                print(" ${getPixelForPrint(j, i)} ")
            }
            println()
        }
    }

    override fun toString(): String {
        return answer.toString()
    }

    /**
     * Паттерн "Фабрика" для изоражений [Image]
     */
    object Factory {

        /**
         * Создает изображение [Image] из [BufferedImage]
         *
         * @param img [BufferedImage] для создания изображения
         * @param answer содержимое изображения, смотри [Image]
         *
         * @return изображение [Image]
         */
        fun create(img: BufferedImage, answer: Int): Image {
            val imageSize = Math.max(img.height, img.width)
            val data = Matrix(0, imageSize)

            for (x in 0..img.width - 1) {
                for (y in 0..img.height - 1) {
                    val clr = img.getRGB(x, y)

                    val alpha = clr shr 24 and 0xff
                    val red = clr and 0x00ff0000 shr 16
                    val green = clr and 0x0000ff00 shr 8
                    val blue = clr and 0x000000ff

                    val clrsum = (255 - ((red + green + blue) / 3)) * (alpha / 255)

                    data.set(x, y, clrsum)
                }
            }

            return Image(data, answer, img.width, img.height)
        }

        /**
         * Создает изображение [Image] из пути к файлу
         *
         * @param fromImgFile путь к файлу в котором находится изображение
         * @param answer содержимое изображения, смотри [Image]
         *
         * @return изображение [Image]
         */
        fun create(fromImgFile: String, answer: Int): Image {
            return create(ImageIO.read(File(fromImgFile)), answer)
        }

        /**
         * Создает изображение [Image] из списка столбцов
         *
         * @param fromColumns столбцы содержаще изображение
         * @param answer содержимое изображения, смотри [Image]
         *
         * @return изображение [Image]
         */
        fun create(fromColumns: List<List<Int>>, answer: Int): Image {
            val height = fromColumns.first().size
            val width = fromColumns.size
            val imageSize = Math.max(width, height)
            val data = Matrix(0, imageSize)

            for (x in 0..width - 1) {
                for (y in 0..height - 1) {
                    data.set(y, x, fromColumns[x][y])
                }
            }

            return Image(data, answer, width, height)
        }
    }

}
