package model

import java.util.*

/**
 * Квадратная матрица, хранящая в себе числа
 *
 * @param T тип числа которое будет храниться в матрице
 * @param withNums какими числами инициализировать матрицу
 * @param size размер матрицы
 */
class Matrix<T : Number>(withNums: T, val size: Int) {

    /**
     * Список чисел в матрице
     */
    val data = ArrayList<T>(size * size)

    init {
        for (i in 1..size * size) {
            data += withNums
        }
    }

    /**
     * Получает значение матрицы
     *
     * @param x значение X-координаты значение с которой нужно получить
     * @param y значение Y-координаты значение с которой нужно получить
     *
     * @return число, содержащееся на ([x],[y])
     */
    fun get(x: Int, y: Int): T {
        return data[(y * size + x)]
    }

    /**
     * Устанавливает значение в матрице
     *
     * @param x значение X-координаты куда надо установить значение [value]
     * @param y значение Y-координаты куда надо установить значение [value]
     * @param value значение которое нужно установить
     */
    fun set(x: Int, y: Int, value: T) {
        data[(y * size + x)] = value
    }

    override fun toString(): String {
        val s = data.foldIndexed("", { i: Int, acc: String, t ->
            acc + if (i.mod(size) == 0) {
                "\n"
            } else {
                ""
            } + t.toInt()
        })
        return s
    }

}