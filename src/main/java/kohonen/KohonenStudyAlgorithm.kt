package kohonen

import base.network.NeuralNetwork
import base.network.NeuronsLayer
import base.studying.StudyAlgorithm
import model.Image


class KohonenStudyAlgorithm(val learningRate: Float) : StudyAlgorithm {


    override fun study(network: NeuralNetwork, images: List<Image>, initializeWeights: Boolean) {
        if (network.layers.size > 3) {
            throw UnsupportedOperationException("Can't teach network with more than 2 layers/")
        }

        weightInit(network)

        images.forEach { img ->
            val correctNeuron = (network.layers.last() as NeuronsLayer).neurons[img.answer]

            (network.layers[1] as NeuronsLayer).neurons.forEachIndexed { i, n ->
                n.connections.find { it.neuron == correctNeuron }?.let {
                    it.weight += learningRate * (img.data.data[i] - it.weight)
                }
            }
        }

    }

    fun weightInit(network: NeuralNetwork) {
        network.layers.map { it as NeuronsLayer }.forEach { it.neurons.forEach { it.connections.forEach { it.weight = 0.01f } } }
    }

}