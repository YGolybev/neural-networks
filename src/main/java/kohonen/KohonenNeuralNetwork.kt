package kohonen

import base.network.BaseNeuralNetwork
import base.network.Layer
import base.network.Neuron
import base.network.NeuronsLayer
import base.studying.StudyAlgorithm
import input.InputLayer
import model.Image
import perceptron.PerceptronLayer


class KohonenNeuralNetwork(layers: List<Layer> = listOf(InputLayer(1),
        PerceptronLayer(11)),
                           override val studyAlgorithm: StudyAlgorithm = KohonenStudyAlgorithm(0.5f)
) : BaseNeuralNetwork(layers) {

    override fun handle(img: Image, toStudy: Boolean): Float {
        (layers.first() as NeuronsLayer).neurons.forEachIndexed { i, neuron ->
            neuron.signal(img.data.data[i].toFloat())
        }

        layers.take(layers.size - 1).forEach(Layer::activate)

        val r = (layers.last() as NeuronsLayer).let {
            it.neurons.indexOf(it.neurons.maxBy(Neuron<Float>::power))
        }

        if (!toStudy) {
            clear()
        }

        return r.toFloat()
    }

}