package preprocess

import java.io.BufferedReader
import java.io.File
import java.io.FileReader

/**
 * Разбирает файл с содержанием изображения
 */
class ImageContent {

    /**
     * Разбирает файл с содержанием изображения
     *
     * @param file файл который необходимо разобрать
     * @return список чисел из файла
     */
    fun preprocess(file: File): List<Int> = BufferedReader(FileReader(file)).lines()
            .reduce { s1, s2 -> s1 + s2 }.get().trim().split(",").map(String::toInt)

}