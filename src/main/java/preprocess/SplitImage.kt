package preprocess

import model.Image
import java.util.*

/**
 * Разделяет изображение на [Image] с символами
 */
class SplitImage {

    /**
     * Разделяет изображение [Image] на символы найденные на нем
     *
     * @param img изображение [Image] которое необхдимо разбить на символы
     * @return список символов на изображении [Image]
     */
    fun preprocess(img: Image): List<Image> {

        val l: List<List<Int>> = (0..img.height - 1)
                .asIterable()
                .map {
                    val a = it * img.width
                    img.data.data.subList(a, a + img.width)
                }.toList()

        val l2 = ArrayList<List<List<Int>>>()
        val l3 = ArrayList<List<Int>>()

        l.forEach {
            if (it.filter { it > 0 }.isNotEmpty()) {
                l3.add(it)
            } else {
                if (l3.isNotEmpty()) {
                    l2.add(l3.toList())
                    l3.clear()
                }
            }
        }

        val listOfNumbers = ArrayList<List<List<Int>>>()
        val number = ArrayList<List<Int>>()

        for (listOfRows in l2) {
            (0..listOfRows.first().size - 1)
                    .map {
                        listOfRows.fold(listOf<Int>(), { acc, element ->
                            acc + element[it]
                        })
                    }
                    .forEach {
                        if (it.filter { it > 0 }.isNotEmpty()) {
                            number.add(it)
                        } else {
                            if (number.isNotEmpty()) {
                                listOfNumbers.add(number.toList())
                                number.clear()
                            }
                        }
                    }
        }

        val listOfLines: List<List<List<Int>>> = listOfNumbers.map { columns ->
            (0..columns.first().size - 1).asIterable().map { i ->
                columns.map { if (i < it.size) it[i] else 0 }
            }
        }

        val numbers = listOfLines.map {
            Image.Factory.create(it.filter { it.filter { it > 0 }.isNotEmpty() }, 0)
        }

        return numbers
    }

}