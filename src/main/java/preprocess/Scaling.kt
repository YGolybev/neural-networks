package preprocess

import model.Image
import model.Matrix

/**
 * Масштабирует изображение, приводя его к размеру [scaleToSize]
 *
 * @param scaleToSize размер изображения на выходе
 */
class Scaling(val scaleToSize: Int) {

    /**
     * Применяет преобразование
     *
     * @param img изображение, которое необхожимо замасштабировать
     * @return замасштабированное изображение
     */
    fun preprocess(img: Image): Image {
        return stretch(img, scaleToSize)
    }

    private fun stretch(sourceImg: Image, size: Int): Image {
        val scale = size.toDouble() / sourceImg.data.size

        val matrix = Matrix(0, size)

        for (j in 0..size - 1) {
            for (i in 0..size - 1) {
                matrix.set(i, j, sourceImg.getPixel((i / scale).toInt(), (j / scale).toInt()))
            }
        }

        return Image(matrix, sourceImg.answer, size, size)
    }

}