package preprocess

import model.Image
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImage
import java.util.*

/**
 * Устраняет перекос на изображении
 */
class Deskew {

    /**
     * Обнаруживает угол наклона текста и устраняет наклон
     *
     * @param image изображение [BufferedImage] с текстом
     *
     * @return изображение [Image] с устраненным перекосом
     */
    fun preprocess(image: BufferedImage): Image {
        val img = Image.Factory.create(image, 0)

        val deg = desk(img).let {
            it.sumByDouble { it.alpha } / it.size
        }

        val transform = AffineTransform()
        transform.rotate(-Math.toRadians(deg), image.width / 2.0, image.height / 2.0)
        val ret =  AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR).filter(image, null)

        return Image.Factory.create(ret, 0)
    }

    private fun desk(img: Image): List<HoughLine> {
        val hls = HashMap<HoughLine, Int>()

        for (y in 0..img.width - 1) {
            for (x in 0..img.height - 1) {
                if (img.getPixel(x, y) >= 1) {
                    if (img.getPixel(x, y + 1) == 0) {
                        for (a in -100..100) {
                            val alpha = a * 0.2
                            val alphaRad = Math.toRadians(alpha)

                            val d = (y * Math.cos(alphaRad) - x * Math.sin(alphaRad)).toInt()

                            val h = HoughLine(alpha, d)

                            val line = hls[h]
                            if (line != null) {
                                hls.put(h, line + 1)
                            } else {
                                hls.put(h, 1)
                            }
                        }
                    }
                }
            }
        }

        return hls.toList().sortedByDescending { it.second }.map { it.first }.take(20)
    }

    private data class HoughLine(val alpha: Double = 0.0, val d: Int = 0) {

        override fun hashCode(): Int {
            return Objects.hash(alpha, d)
        }

        override fun equals(other: Any?): Boolean {
            return when (other) {
                is HoughLine -> other.alpha == alpha && other.d == d
                else -> super.equals(other)
            }
        }
    }
}