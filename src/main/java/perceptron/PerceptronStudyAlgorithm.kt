package perceptron

import base.network.NeuralNetwork
import base.network.NeuronsLayer
import base.studying.StudyAlgorithm
import model.Image


class PerceptronStudyAlgorithm : StudyAlgorithm {

    override fun study(network: NeuralNetwork, images: List<Image>, initializeWeights: Boolean) {
        with(network) {
            images.forEach { img ->
                handle(img, true)
                val answerNeuron = (layers.last() as NeuronsLayer).neurons[img.answer]

                (layers[layers.lastIndex - 1] as NeuronsLayer).neurons.forEach {
                    val con = it.connections.find { c -> c.neuron === answerNeuron }

                    con?.let { c ->
                        with(c) {
                            //                            weight += 0.5 * (it.activation() - weight)
                        }
                    }
                }

                clear()
            }
        }
    }

}