package perceptron

import base.network.BaseNeuralNetwork
import base.network.Layer
import base.studying.StudyAlgorithm
import input.InputLayer
import study.BackpropagationStudyAlgorithm


class Perceptron(layers: List<Layer> = listOf(
        InputLayer(18 * 18),
        PerceptronLayer(18 * 18),
        PerceptronLayer(10)
), override val studyAlgorithm: StudyAlgorithm = BackpropagationStudyAlgorithm())
    : BaseNeuralNetwork(layers)