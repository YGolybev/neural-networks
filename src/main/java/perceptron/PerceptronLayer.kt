package perceptron

import base.network.NeuronsLayer


class PerceptronLayer(size: Int = 0) : NeuronsLayer({ PerceptronNeuron() }) {

    init {
        fillWith(size)
    }

}