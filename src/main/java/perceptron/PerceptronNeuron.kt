package perceptron

import afunctions.SigmoidFunction
import base.network.BaseNeuron


class PerceptronNeuron(power: Float = 0f) : BaseNeuron(power, SigmoidFunction())