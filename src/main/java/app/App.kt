package app

import cnn.ConvolutionalNeuralNetwork
import cnn.convolution.ConvLayer
import model.Image
import preprocess.ImageContent
import preprocess.Scaling
import preprocess.SplitImage
import java.io.File

fun main(args: Array<String>) {

    val s = SplitImage()
    val scaling = Scaling(18)

    val imgsFromFile = s.preprocess(Image.Factory.create("C:\\development\\misc\\train_numbers.bmp", 0))
    val answers = ImageContent().preprocess(File("C:\\development\\misc\\train_numbers_content.txt"))

    val imgs = imgsFromFile.mapIndexed { i, img ->
        scaling.preprocess(Image(img.data, answers[i], img.data.size, img.data.size))
    }

    val testImgsFromFile = s.preprocess(Image.Factory.create("C:\\development\\misc\\test_numbers.bmp", 0))
    val testAnswers = ImageContent().preprocess(File("C:\\development\\misc\\test_numbers_content.txt"))

    val testImgs = testImgsFromFile.mapIndexed { i, img ->
        scaling.preprocess(Image(img.data, testAnswers[i], img.data.size, img.data.size))
    }

    println("START")

    val cnn = ConvolutionalNeuralNetwork()

    testOn(cnn, imgs, testImgs)
}


fun testOn(cnn: ConvolutionalNeuralNetwork, trainSet: List<Image>, testSet: List<Image>) {
    cnn.study(trainSet, true)

    repeat(25) {
        print("Testing... ")
        val rightAnswers = trainSet.fold(0) { acc, img ->
            acc + if (cnn.handle(img).toInt() == img.answer) 1 else 0
        }
        println("RightAnswers: $rightAnswers of ${trainSet.size} ")

        print("Incorrect of ${testSet.size}: ")
        testSet.forEach {
            val ans = cnn.handle(it).toInt()
            if (ans != it.answer) {
                print("$ans : ${it.answer} | ")
            }
        }
        println()

        cnn.study(trainSet)
    }

    (cnn.layers[5] as ConvLayer).fs.forEach(::println)

    val a = 1
}