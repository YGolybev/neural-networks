package app

import base.BACKPROPAGATION
import base.KOHONEN
import base.NeuralNetworkTesterBuilder
import cnn.ConvolutionalNeuralNetwork
import kohonen.KohonenNeuralNetwork
import misc.LayerParser
import perceptron.Perceptron
import java.io.File

const val incorrectInput = "Некорректный ввод"

fun main(args: Array<String>) {

    println("Распознавание цифр в машинописном тексте")
    println("Выберите тип тестируемой нейронной сети:")
    println("per - Персептрон, con - Сверточная нейронная сеть, koh - Нейронная сеть Кохонена")

    var builder = NeuralNetworkTesterBuilder()

    builder = handleInput(builder, { builder.neuralNetwork(it) })

    println("Выберите скорость обучения (рекомендуется число между 0.01 и 1)")
    builder = handleInput(builder, { builder.learningRate(it.toFloat()) })

    builder = when (builder.neuralNetwork) {
        is KohonenNeuralNetwork -> builder.studyAlgorithm(KOHONEN)
        else -> builder.studyAlgorithm(BACKPROPAGATION)
    }

    builder = when (builder.neuralNetwork) {
        is ConvolutionalNeuralNetwork -> {
            println("Выберите топологию сети")
            println("Для сверточной нейронной сети введите топологию, следующим образом:")
            println("[Тип_слоя]" +
                    "[:Когда [Тип_слоя]: Сверточный, то Размер фильтра; \n" +
                    "Субдискретизирующий, то размер области субдискретизации; \n " +
                    "ReLU, то ничего; \n" +
                    "Полностью соединенный, то количество нейронов;] \n" +
                    "[:Когда [Тип_слоя] Сверточный, то количество фильтров;]\n" +
                    "Разделять слои необходимо символов \",\" \n" +
                    "Пример: C:4:2,R,C:4:4,S:2,C:4:8,F:8 ")

            handleInput(builder, { builder.layers(LayerParser.parser(it)) })
        }
        is Perceptron -> {
            println("Выберите топологию сети")
            println("Для персептрона введите топологию, следующим образом:")
            println("F:[Количество нейронов на слое] " +
                    "Пример: F:10,F:20")

            handleInput(builder, { builder.layers(LayerParser.parser(it)) })
        }
        else -> builder
    }

    println("Выберите размер изображения в выборках:")
    builder = handleInput(builder, { builder.imageSize(it.toInt()) })

    println("Выберите файл содержащий обучающую выборку:")
    builder = handleInput(builder, { builder.trainSet(File(it)) })

    println("Выберите файл содержащий числа из обучающей выборки:")
    builder = handleInput(builder, { builder.trainSetContent(File(it)) })

    println("Выберите файл содержащий выборку для тестирования:")
    builder = handleInput(builder, { builder.testSet(File(it)) })

    println("Выберите файл содержащий числа из выборки для тестирования:")
    builder = handleInput(builder, { builder.testSetContent(File(it)) })

    if (builder.neuralNetwork !is KohonenNeuralNetwork) {
        println("Выберите когда будет остановлена тренировка:\n" +
                "E:[Целое_число] - обучать некоторое количество эпох, заданному числом \n" +
                "M:[Число_с_плавающей_точкой] - обучать, пока среднеквадратическая ошибка не будет менее или равна числу")
        builder = handleInput(builder, {
            val s = it.trim().toLowerCase().split(":")
            when (s[0]) {
                "e" -> builder.epochs(s[1].toInt())
                "m" -> builder.mse(s[1].toFloat())
                else -> throw UnsupportedOperationException(incorrectInput)
            }
        })
    } else {
        builder = builder.epochs(1)
    }


    println("Включить устранение перекоса, на тестовом изображении? Y\\N")
    builder = handleInput(builder, { builder.deskew(it.trim().toLowerCase() == "y") })

    val tester = try {
        builder.build()
    } catch (ex: Exception) {
        ex.printStackTrace()
        println("Невозможно создать нейронную сеть или загрузить выборки, проверьте введенные данные")
        null
    }
    
    try {
        print(tester?.test() ?: "")
    } catch (ex: Exception) {
        println("Произошел сбой во время обучения, возможно введенные данные некорректны.")
    }

}

fun handleInput(nnb: NeuralNetworkTesterBuilder, input: (String) -> NeuralNetworkTesterBuilder): NeuralNetworkTesterBuilder {
    var correct = false
    var builder = nnb
    while (!correct) {
        val s = readLine() ?: ""
        try {
            builder = input(s)
            correct = true
        } catch (ex: UnsupportedOperationException) {
            println(ex.message)
        } catch (ex: Exception) {
            println(incorrectInput)
        }
    }

    return builder
}

